FROM maven:3.8.6-openjdk-11-slim

ENV ENV_DB_HOST=""
ENV ENV_DB_DATABASE=""
ENV ENV_DB_USER=""
ENV ENV_DB_PASSWORD=""

COPY app /app
WORKDIR /app
RUN mvn clean package
RUN mv target/*.jar app.jar
ENTRYPOINT ["java", "-DDB_USERNAME=${ENV_DB_USER}", "-DDB_PASSWORD=${ENV_DB_PASSWORD}", "-DDB_URL=jdbc:mariadb://${ENV_DB_HOST}:3306/${ENV_DB_DATABASE}", "-jar","app.jar"]
