# Ref-card-03
Ref-Card-03 is an application for me to test out how to make a running environment that contains a simple Java program that connects to a database. All this is contained inside of a Docker Container

## Variables
To deploy this project, you will need a few variables. These are the following:
<pre>
- AWS_ACCESS_KEY_ID*
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY*
- AWS_SESSION_TOKEN*
- CI_AWS_ECR_REGISTRY
- CI_AWS_ECR_REPOSITORY_NAME
</pre>

Variables marked with * have to be set every time the lab restarts and they can be found in the learner lab under "AWS Details".</br>
AWS_DEFAULT_REGION is the reagion where your AWS is being hosted</br>
CI_AWS_ECR_REGISTRY holds the location of your ECR registry on AWS</br>
CI_AWS_ECR_REPOSITORY_NAME holds the name of your repository on AWS</br>

## Environment variables

You need to create three environment variables inside of ECS:
- DB_URL defines where the database is
- DB_USERNAME defines the username to use for the database
- DB_PASSWORD is the password for the database
